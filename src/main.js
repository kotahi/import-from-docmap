const { getDocMapsFromDataHub } = require('./network')

const processDocmaps = require('./conversion')

const {
  labels: { PLUGIN_TAG },
} = require('./constants')

const doImport =
  (
    getStubManuscriptObject,
    hasManuscriptWithDoi,
    getSubmissionForm,
    useTransaction,
    logger,
  ) =>
  async () => {
    return useTransaction(async trx => {
      logger.info(`${PLUGIN_TAG} retrieving data from Data Hub`)
      const retrievedDocMaps = await getDocMapsFromDataHub(logger)

      logger.info(`${PLUGIN_TAG} gathering configured submission form info`)
      const submissionForm = await getSubmissionForm({ trx })

      logger.info(`${PLUGIN_TAG} getting manuscript's blueprint`)
      const manuscriptBlueprint = await getStubManuscriptObject({ trx })

      logger.info(`${PLUGIN_TAG} start processing docmaps`)
      return processDocmaps(
        retrievedDocMaps,
        submissionForm,
        manuscriptBlueprint,
        hasManuscriptWithDoi,
        logger,
        { trx },
      )
    })
  }

module.exports = doImport

const Ajv = require('ajv')
const addFormats = require('ajv-formats')
const uniq = require('lodash/uniq')

const {
  statuses: {
    ASSERTION_STATUS_UNDER_REVIEW,
    ASSERTION_STATUS_PEER_REVIEWED,
    ASSERTION_STATUS_REVISED,
    OUTPUT_TYPE_PREPRINT,
    OUTPUT_TYPE_EVALUATION_SUMMARY,
    OUTPUT_TYPE_REVIEW_ARTICLE,
  },
  labels: { PLUGIN_TAG },
} = require('./constants')

const { schema } = require('./schema')

const validateDataBasedOnSchema = data => {
  const ajv = new Ajv()
  addFormats(ajv)

  const isValidData = ajv.validate(schema, data)

  if (!isValidData) {
    throw new Error(ajv.errors)
  }

  return isValidData
}

const getFirstStepKey = docmap => docmap['first-step']
const getStepByKey = (docmap, stepKey) => docmap.steps[stepKey]
const getDocmapStepIds = docmap => Object.keys(docmap.steps)

// eslint-disable-next-line no-unused-vars
const getStepAssertionByStatus = (assertions, status) =>
  assertions.find(assertion => assertion.status === status)

const getHappenedStepAssertions = assertions =>
  assertions.find(assertion => assertion.happened)

const getStepActionOutputByType = (action, outputType) =>
  action.outputs.find(output => output.type === outputType)

const hasActionOutput = (actions, outputType) => {
  let found = false
  actions.forEach(action => {
    const exists = action.outputs.find(output => output.type === outputType)

    if (exists) {
      found = true
    }
  })
  return found
}

const getDOIfromFirstStepInputs = (docmap, logger) => {
  const firstStepId = getFirstStepKey(docmap)

  const input = docmap.steps[firstStepId].inputs.find(
    i => i.type === OUTPUT_TYPE_PREPRINT,
  )

  if (!input) {
    logger.error(
      `${PLUGIN_TAG} output of type ${OUTPUT_TYPE_PREPRINT} not found in first step`,
    )
  }

  return input.doi
}

const getDOIfromStepInputs = (step, logger) => {
  const { inputs } = step

  const input = inputs.find(i => i.type === OUTPUT_TYPE_PREPRINT)

  if (!input) {
    logger.error(
      `${PLUGIN_TAG} output of type ${OUTPUT_TYPE_PREPRINT} not found in this step`,
    )
  }

  return input.doi
}

const getDOIsFromDocmap = (docmap, logger) => {
  const steps = Object.values(docmap.steps)
  const dois = []

  steps.forEach(({ inputs }) => {
    const input = inputs.find(i => i.type === OUTPUT_TYPE_PREPRINT)

    dois.push(input?.doi || null)
  })

  return uniq(dois.filter(Boolean))
}

const getDocmapIdentifier = step => {
  let foundIdentifier
  const { actions } = step

  actions.forEach(action => {
    const preprintOutputAction = getStepActionOutputByType(
      action,
      OUTPUT_TYPE_PREPRINT,
    )

    if (preprintOutputAction) {
      foundIdentifier = preprintOutputAction.identifier
    }
  })

  return foundIdentifier
}

const docmapsCleaner = async (docmaps, hasManuscriptWithDoi, options = {}) => {
  const result = []

  const filtered = docmaps.filter(docmap => {
    // loop through all steps
    const steps = Object.values(docmap.steps)

    let docmapHasPreprint = false
    let docmapHasBeenReviewed = false

    steps.forEach(step => {
      const hasStepHappened = getHappenedStepAssertions(step.assertions)

      if (!hasStepHappened) return

      if (hasActionOutput(step.actions, OUTPUT_TYPE_PREPRINT)) {
        docmapHasPreprint = true
      }

      const hasValidOutput =
        hasActionOutput(step.actions, OUTPUT_TYPE_REVIEW_ARTICLE) ||
        hasActionOutput(step.actions, OUTPUT_TYPE_EVALUATION_SUMMARY)

      if (hasValidOutput) {
        docmapHasBeenReviewed = true
      }
    })

    if (docmapHasPreprint && !docmapHasBeenReviewed) {
      return false
    }

    return docmapHasPreprint
  })

  await Promise.all(
    filtered.map(async item => {
      const DOI = getDOIfromFirstStepInputs(item)

      const manuscriptWithDOIExists = await hasManuscriptWithDoi(
        generateURLBasedOnDOI(DOI),
        true,
        options,
      ) // TODO: check why the second argument
      // see: https://gitlab.coko.foundation/kotahi/kotahi/-/blob/main/packages/server/server/plugins/broker.js#L117

      if (!manuscriptWithDOIExists) {
        result.push(item)
      }
    }),
  )

  return result
}

const generateURLBasedOnDOI = DOI => `https://doi.org/${DOI}`

const shouldCreateManuscript = async step => {
  const { assertions, actions } = step

  const hasHappenedAssertion = !!getHappenedStepAssertions(assertions)

  const stepAssertions = getStepAssertionByStatus(
    assertions,
    ASSERTION_STATUS_UNDER_REVIEW,
  )

  const hasActionOutputPreprint = hasActionOutput(actions, OUTPUT_TYPE_PREPRINT)

  return hasHappenedAssertion && stepAssertions && hasActionOutputPreprint
}

const shouldExtractReviewsAndDecision = async step => {
  const { assertions } = step

  const hasHappenedAssertion = !!getHappenedStepAssertions(assertions)

  const peerReviewedAssertion = getStepAssertionByStatus(
    assertions,
    ASSERTION_STATUS_PEER_REVIEWED,
  )

  const revisedAssertion = getStepAssertionByStatus(
    assertions,
    ASSERTION_STATUS_REVISED,
  )

  return hasHappenedAssertion && (peerReviewedAssertion || revisedAssertion)
}

const getSubmissionFormTitleType = async submissionFormChildren => {
  const field = submissionFormChildren.find(
    child => child.name === 'submission.$title',
  )

  return field.component === 'TextField' ? field.component : 'other'
}

module.exports = {
  docmapsCleaner,
  generateURLBasedOnDOI,
  getDocmapIdentifier,
  getDocmapStepIds,
  getDOIfromFirstStepInputs,
  getDOIfromStepInputs,
  getDOIsFromDocmap,
  getStepByKey,
  getSubmissionFormTitleType,
  shouldCreateManuscript,
  shouldExtractReviewsAndDecision,
  validateDataBasedOnSchema,
}

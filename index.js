const doImport = require('./src/main')

const pluginEntry = async broker => {
  const { logger, useTransaction } = broker
  await broker.addManuscriptImporter(
    'any', // 'manual', 'automatic' or 'any'
    doImport(
      broker.getStubManuscriptObject,
      broker.hasManuscriptWithDoi,
      broker.getSubmissionForm,
      useTransaction,
      logger,
    ),
  )
}

module.exports = pluginEntry
